# RStudio in singularity

RStudio packaged in a Singularity container suitible for running from inside
the protected network on a server shared by several researchers. 

## Issues

When running multiple containers, there will be trouble if
they all try to listen on the default port. To avoid this, we want to locate an unused 
port at runtime so that multiple containers do not attempt to grab the same port at 
the same time.

RStudio also expects to write to several file to /tmp to maintainer state, and since 
Singularity by default binds the host server's /tmp to each user's container there can 
be trouble if you run multiple containers simultaneously. 

### Dynamic port allocation

To work around the port collision issue, we run a bash shell script 
````
findport 
```` 
to find an open port and update the /etc/rstudio/rserver.conf in the container with that port
before launching the RStudio server.

### Private RStudio /tmp files

So that users do not step on each other's RStudio /tmp files, we bind 3 external
directories to the container when we launch an instance. The script
````
run-rstudio
````
cleans up any leftovers from previous sessions, and makes sure that there are appropriate
directories in 
````
/tmp/$THISUSER-rstudio
````
to hold the bind mounts we will make when running the container. The container is then launched
with this command
````
singularity exec -B /tmp/$THISUSER-rstudio/rstudio-server:/tmp/rstudio-server/ \
                 -B /tmp/$THISUSER-rstudio/etc-rstudio:/etc/rstudio \
                 -B /tmp/$THISUSER-rstudio/tmp-rsession:/tmp/rstudio-rsession \
                  /home/weardukexfer/singularity-r.simg bash findport
````
which calls the bash shell script findport to configre the port the RStudio server will listen on.
Note that the findport shell script must be in the user's home directory. To make sure this is true
the run-rstudio command copies it from a safe place (/home/weardukexfer/findport in the case of the
wearduke project.)



