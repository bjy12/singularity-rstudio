BootStrap: docker
From: ubuntu:18.04

%labels
  Maintainer Mark McCahill
  R_Version 3.6.0

%help
  This will run RStudio Server

%apprun rserver
  exec rserver "${@}"

%runscript
  exec rserver "${@}"

%environment
  export PATH=/usr/lib/rstudio-server/bin:${PATH}

%setup
  install -Dv \
    rstudio_auth.sh \
    ${SINGULARITY_ROOTFS}/usr/lib/rstudio-server/bin/rstudio_auth
  install -Dv \
    ldap_auth.py \
    ${SINGULARITY_ROOTFS}/usr/lib/rstudio-server/bin/ldap_auth

  mkdir ${SINGULARITY_ROOTFS}/r-install-scripts

%files
    r-install-scripts/* /r-install-scripts/

%post
  # Software versions
  apt-get update
  apt-get install -y \
    gnupg2 \
    apt-utils

  echo  "deb http://cran.rstudio.com/bin/linux/ubuntu bionic-cran35/"  >>  /etc/apt/sources.list
  DEBIAN_FRONTEND=noninteractive apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9

  apt-get update
  apt-get dist-upgrade -y

  # Get dependencies
  apt-get install -y \
   libopenblas-base \
   r-base \
   r-base-dev \
   vim \
   less \
   net-tools \
   inetutils-ping \
   curl \
   git \
   telnet \
   nmap \
   socat \
   software-properties-common \
   gdebi-core \
   libapparmor1 \
   wget \
   net-tools \
   iproute2 \
   sudo 

  # Configure default locale
  apt-get install  -y locales 
  locale-gen en_US en_US.UTF-8
  DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales

  add-apt-repository "deb http://archive.ubuntu.com/ubuntu $(lsb_release -sc) main universe restricted multiverse"
  apt-get update --fix-missing

  # we need gdal > 2
  add-apt-repository ppa:ubuntugis/ppa
  apt-get update


  DEBIAN_FRONTEND=noninteractive apt-get install -y \
   libcurl4-gnutls-dev \
   libgit2-dev \
   libxml2-dev \
   libssl-dev \
   libudunits2-dev \
   libpoppler-cpp-dev \
   texlive \
   texlive-base \
   texlive-latex-extra \
   texlive-pstricks \
   pandoc \
   texlive-publishers \
   texlive-fonts-extra \
   texlive-latex-extra \
   texlive-humanities \
   lmodern \
   libxml2  \
   libxml2-dev  \
   libssl-dev \
   libudunits2-0  \
   libudunits2-dev \
   software-properties-common 


  # Add a default CRAN mirror
  echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl')" >> /usr/lib/R/etc/Rprofile.site

  # Add a directory for host R libraries
  mkdir -p /library
  echo "R_LIBS_SITE=/library:\${R_LIBS_SITE}" >> /usr/lib/R/etc/Renviron.site
   
  DEBIAN_FRONTEND=noninteractive wget --no-verbose https://s3.amazonaws.com/rstudio-ide-build/server/trusty/amd64/rstudio-server-1.2.907-amd64.deb
  DEBIAN_FRONTEND=noninteractive gdebi --n rstudio-server-1.2.907-amd64.deb
  rm rstudio-server-1.2.907-amd64.deb

  # add rserver config file for RStudio and make it writeable
  # so we can assign a port for the server at container runtime
  cp /dev/null /etc/rstudio/rserver.conf
  chmod ugo+rw /etc/rstudio/rserver.conf

  # update the R packages we will need for knitr
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages(c("xfun", "bitops", "caTools", "digest", "Rcpp", "htmltools", "yaml", "stringi", "magrittr"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages(c("mime", "glue", "stringr", "highr", "formatR", "evaluate", "markdown", "xfun", "knitr"), repos="http://cran.us.r-project.org",quiet=TRUE)'


  # R packages we need for devtools - and we need devtools to be able to update the rmarkdown package
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("processx", "ps", "callr", "crayon", "assertthat", "cli", "desc"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("prettyunits", "backports", "rprojroot", "withr", "pkgbuild", "rlang"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("rstudioapi", "pkgload", "rcmdcheck", "remotes", "xopen", "clipr"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("clisymbols", "sessioninfo", "purrr", "usethis", "sys", "askpass"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("openssl", "brew", "roxygen2", "fs", "gh", "rversions", "git2r"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("devtools", "R6", "httr", "RCurl", "BH", "xml2", "curl", "jsonlite"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("ini", "downloader", "memoise", "plyr", "XML", "whisker", "nloptr"), repos="http://cran.us.r-project.org",quiet=TRUE)'

  # libraries Eric Green wanted
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages(c("processx", "ps", "callr", "crayon", "assertthat", "cli", "desc", "prettyunits"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("backports", "rprojroot", "withr", "pkgbuild", "rlang", "rstudioapi"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("pkgload", "rcmdcheck", "remotes", "xopen", "clipr", "clisymbols"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("sessioninfo", "purrr", "usethis", "sys", "askpass", "openssl", "brew"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("roxygen2", "fs", "gh", "rversions", "git2r", "devtools", "R6", "httr"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("RCurl", "BH", "xml2", "curl", "jsonlite", "ini", "downloader", "memoise"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("plyr", "XML", "whisker" ), repos="http://cran.us.r-project.org",quiet=TRUE)'

# more libraries Mine Cetinakya-Rundel asked for
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("openintro", "bindr", "bindrcpp", "plogr", "tidyselect", "dplyr", "DBI"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  
  DEBIAN_FRONTEND=noninteractive R --vanilla --quiet -e 'install.packages( c("chron", "data.table", "rematch", "cellranger", "tidyr", "googlesheets", "hms", "readr", "selectr", "rvest", "pbkrtest"), repos="http://cran.us.r-project.org",quiet=TRUE)'
  

  # Shiny
  wget https://download3.rstudio.org/ubuntu-14.04/x86_64/shiny-server-1.5.7.907-amd64.deb
  DEBIAN_FRONTEND=noninteractive gdebi -n shiny-server-1.5.7.907-amd64.deb
  rm shiny-server-1.5.7.907-amd64.deb
  R CMD BATCH /r-install-scripts/install-Shiny.R
  rm /install-Shiny.Rout


  # Add support for LDAP authentication
  wget \
    --no-verbose \
    -O get-pip.py \
    "https://bootstrap.pypa.io/get-pip.py"
  python3 get-pip.py
  rm -f get-pip.py
  pip3 install ldap3

  # Clean up
  rm -rf /var/lib/apt/lists/*

